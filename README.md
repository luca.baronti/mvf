# MVF - Multivariate Test Functions Library

Unofficial Multivariate Test Functions Library repository.
This library code is taken from the following pubblication:

- MLA	Adorio, Ernesto P., and U. Diliman. ["Mvf-multivariate test functions library in c for unconstrained global optimization."](http://www.geocities.ws/eadorio/mvf.pdf) Quezon City, Metro Manila, Philippines (2005): 100-104.

The intent of this repository is to provide a ready-to-use repository for this library.
Please cite the original author's paper if you are using this library in your research. 
Here's the bibtex format:
```latex
@article{adorio2005mvf,
  title={Mvf-multivariate test functions library in c for unconstrained global optimization},
  author={Adorio, Ernesto P and Diliman, U},
  journal={Quezon City, Metro Manila, Philippines},
  pages={100--104},
  year={2005}
}
```
Both the library code and the code to build the python wrapper can be found in the paper, I just added a CMake script to build it as a shared C object.

If you are interested in a benchmark functions collection entirely wrote in Python, with extra features, and in ongoing development feel free to check out [this other project](https://gitlab.com/luca.baronti/python_benchmark_functions).
## Building
To build the library just use the following commands:
```
$ cmake .
$ make
```
to build the python wrapper the following command is also needed
```
$ make python_wrapper
```
## Available Functions
All the available functions takes exactly two parameters:

- **int n** the number of dimensions
- **double * x** the point, as a double array
 
and returns the function value on the given point, as a double.
The available functions are the following ones:

- **mvfAckley**
- **mvfBeale**
- **mvfBohachevsky1**
- **mvfBohachevsky2**
- **mvfBooth**
- **mvfBoxBetts**
- **mvfBranin**
- **mvfBranin2**
- **mvfCamel3**
- **mvfCamel6**
- **mvfChichinadze**
- **mvfCola**
- **mvfColville**
- **mvfCorana**
- **mvfEasom**
- **mvfEggholder**
- **mvfExp2**
- **mvfFraudensteinRoth**
- **mvfGear**
- **mvfGeneralizedRosenbrock**
- **mvfGoldsteinPrice**
- **mvfGriewank**
- **mvfHansen**
- **mvfHartman3**
- **mvfHartman6**
- **mvfHimmelblau**
- **mvfHolzman1**
- **mvfHolzman2**
- **mvfHosaki**
- **mvfHyperellipsoid**
- **mvfKatsuuras**
- **mvfKowalik**
- **mvfLangerman**
- **mvfLennardJones**
- **mvfLeon**
- **mvfLevy**
- **mvfMatyas**
- **mvfMaxmod**
- **mvfMcCormick**
- **mvfMichalewitz**
- **mvfMultimod**
- **mvfNeumaierPerm**
- **mvfNeumaierPerm0**
- **mvfNeumaierPowersum**
- **mvfNeumaierTrid**
- **mvfOddsquare**
- **mvfPaviani**
- **mvfPlateau**
- **mvfPowell**
- **mvfQuarticNoiseU**
- **mvfQuarticNoiseZ**
- **mvfRana**
- **mvfRastrigin**
- **mvfRastrigin2**
- **mvfRosenbrock**
- **mvfSchaffer1**
- **mvfSchaffer2**
- **mvfSchwefel1_2**
- **mvfSchwefel2_21**
- **mvfSchwefel2_22**
- **mvfSchwefel2_26**
- **mvfShekel2**
- **mvfShekel4_5**
- **mvfShekel4_7**
- **mvfShekel4_10**
- **mvfShekel10**
- **mvfShubert**
- **mvfShubert2**
- **mvfShubert3**
- **mvfSphere**
- **mvfSphere2**
- **mvfStep**
- **mvfStretchedV**
- **mvfSumSquares**
- **mvfTrecanni**
- **mvfTrefethen4**
- **mvfWatson**
- **mvfXor**
- **mvfZettl**
- **mvfZimmerman**

