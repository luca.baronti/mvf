%module mvf
%{
#include "mvf.h"
#include "rnd.h"
%}
%include "carrays.i"
%array_class(double, doubleArray);
%include mvf.h
%include rnd.h
